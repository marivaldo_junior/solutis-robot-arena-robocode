# Solutis Robot Arena Robocode

## Robô Schwarzenegger (Homenagem ao Exterminador do Futuro).

Um simples robô do [Robocode](https://robocode.sourceforge.io) que tenta pré-determinar a localização do inimigo para efetuar seus disparos, baseado em tutorias da [RoboWiki](https://robowiki.net/wiki/Robocode).

### Pontos Fortes
- Movimentação constante o deixa menos vunerável a disparos definidos apenas pelo local de encontro e não pela movimentação.
- Seus disparos baseados na predição dos movimentos do inimigo são efetivos em tanto em robos estáticos quanto os que se movimentam constantemente. 
### Pontos Fracos
- Movimentação simples.
- Não muito efetivo contra diversos inimigos.

Desenvolvido para a Solutis Robot Arena por Marivaldo Pereira de Oliveira Junior.

Obs. espero que meu robô dure tanto quanto a franquia de filmes.

