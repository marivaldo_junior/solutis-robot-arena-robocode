package TalentSolutis.robot;

import TalentSolutis.enemy.AdvancedEnemyBot;
import robocode.*;
import java.awt.geom.Point2D;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Schwarzenegger - a robot by (Marivaldo Junior)
 */
public class Schwarzenegger extends AdvancedRobot
{

	private AdvancedEnemyBot enemy = new AdvancedEnemyBot();

	public void run() {
		setAdjustRadarForGunTurn(true);
		setAdjustGunForRobotTurn(true);
		enemy.reset();
		setTurnRadarRight(360);

		while (true) {
			// girar o radar
			setTurnRadarRight(360);
			// girar o robo
			setTurnRight(5);
			setAhead(20);
			//ação de ataque preventivo.
			doGun();
			// repetir açoes
			execute();
		}
	}
	//Ataque
	void doGun() {

		// Não atira se não tiver inimigos
		if (enemy.none())
			return;

		// determina o poder de fogo/firepower baseado na distancia/Distance
		double firePower = Math.min(500 / enemy.getDistance(), 3);
		// determina a velocidade da bala/bulletSpeed
		double bulletSpeed = 20 - firePower * 3;
		long time = (long)(enemy.getDistance() / bulletSpeed);

		// determina o movimento da arma para a localização prevista.
		double futureX = enemy.getFutureX(time);
		double futureY = enemy.getFutureY(time);
		double absDeg = absoluteBearing(getX(), getY(), futureX, futureY);

		// movimenta a arma para a localização prevista
		setTurnGunRight(normalizeBearing(absDeg - getGunHeading()));

		// atira se a arma estiver fria e na direção correta.
		if (getGunHeat() == 0 && Math.abs(getGunTurnRemaining()) < 10) {
			setFire(firePower);
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {

		// rastreia se não tiver inimigos/se esta proximo/validando alvo.
		if ( enemy.none() || e.getDistance() < enemy.getDistance() - 70 ||
				e.getName().equals(enemy.getName())) {

			// rastreando
			enemy.update(e, this);
		}
	}
	// determina o rolamento em dois pontos.
	double absoluteBearing(double x1, double y1, double x2, double y2) {
		double xo = x2-x1;
		double yo = y2-y1;
		double hyp = Point2D.distance(x1, y1, x2, y2);
		double arcSin = Math.toDegrees(Math.asin(xo / hyp));
		double bearing = 0;

		if (xo > 0 && yo > 0) { 
			bearing = arcSin;
		} else if (xo < 0 && yo > 0) { 
			bearing = 360 + arcSin; 
		} else if (xo > 0 && yo < 0) { 
			bearing = 180 - arcSin;
		} else if (xo < 0 && yo < 0) { 
			bearing = 180 - arcSin; 
		}

		return bearing;
	}

	// realiza um rolamento entre +180 e -180
	double normalizeBearing(double angle) {
		while (angle >  180) angle -= 360;
		while (angle < -180) angle += 360;
		return angle;
	}
	public void onRobotDeath(RobotDeathEvent e) {
		// verifica se o robo que estava sendo rastreado morreu.
		if (e.getName().equals(enemy.getName())) {
			enemy.reset();
		}
	}   
}