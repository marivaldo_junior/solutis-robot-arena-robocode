package TalentSolutis.enemy;
import robocode.Robot;
import robocode.ScannedRobotEvent;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html
//Classe auxiliar para manipular os dados do inimigo.
//Tornando a classe do robo menos complexa.


public class AdvancedEnemyBot extends EnemyBot{

    private double x;
    private double y;

    public void reset() {
        super.reset();

        x = 0.0;
        y = 0.0;
    }

    public void update(ScannedRobotEvent e, Robot robot) {
        super.update(e);

        double absBearingDeg = (robot.getHeading() + e.getBearing());
        if (absBearingDeg < 0) absBearingDeg += 360;

        x = robot.getX() + Math.sin(Math.toRadians(absBearingDeg)) * e.getDistance();
        y = robot.getY() + Math.cos(Math.toRadians(absBearingDeg)) * e.getDistance();
    }

    public double getFutureX(long when){
        return x + Math.sin(Math.toRadians(getHeading())) * getVelocity() * when;
    }

    public double getFutureY(long when){
        return y + Math.cos(Math.toRadians(getHeading())) * getVelocity() * when;
    }

    public double getFutureT(Robot robot, double bulletVelocity){

        // velocidade do inimigo
        double v_E = getVelocity();

        double x_diff = x - robot.getX();
        double y_diff = y - robot.getY();

        // angulo do destino do inimigo
        double sin = Math.sin(Math.toRadians(getHeading()));
        double cos = Math.cos(Math.toRadians(getHeading()));

        // tempo
        double T;
        double v_B = bulletVelocity;

        double xy = (x_diff*sin + y_diff*cos);

        T = ( (v_E*xy) + Math.sqrt(sqr(v_E)*sqr(xy) + (sqr(x_diff) + sqr(y_diff))*(sqr(v_B) + sqr(v_E))) ) / (sqr(v_B) - sqr(v_E));

        return T;

    }

    private static double sqr(double in){
        return in * in;
    }
}